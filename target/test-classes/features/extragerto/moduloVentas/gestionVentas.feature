@gestionVentas
Feature: Gestión de módulo de ventas extragerto


  @creacionCliente
  Scenario: Creación de cliente
    Given ingreso al aplicativo "https://app.extragerto.com/"
    And ingresamos a la sesion de empresa con el usuario "jdmaury" y la contrasenia "mindfreak7"
    And ingresamos a la sesion de usuario con el usuario "admin" y la contrasenia "master"
    When ingresamos al modulo de ventas/clientes
    And diligenciamos la informacion del cliente
    |TipoDoc|NumDocumento|FormaPago|Plantilla|Estado|RazonSocial|NombreComercial|Contacto     |Direccion    |Ciudad|Telefono|Celular    |Correo                     |Vendedor  |ListaPrecio|Zona|
    |CC     |5765004945 |15 |Template |Inactivo|Choucair   |CHC Testing    |Mayra Perdomo|Calle 1A#23-3|BOG|3700320 |30024687861|jmauryn@choucairtesting.com|MAY|Lista 1    |2   |
    And creamos el cliente
    And validamos mensaje de confirmacion
    And verificamos que exista usuario con cedula "1.045.721.531" en la grilla de clientes
    
    
  @edicionCliente
  Scenario: Modificacion de cliente y adición de contacto
  	Given ingreso al aplicativo "https://app.extragerto.com/"
    And ingresamos a la sesion de empresa con el usuario "jdmaury" y la contrasenia "mindfreak7"
    And ingresamos a la sesion de usuario con el usuario "admin" y la contrasenia "master"
    When ingresamos al modulo de ventas/clientes
    And ingresamos al cliente con cedula "1.039.349.548" 
    And editamos la informacion del cliente
    |TipoDoc|NumDocumento|FormaPago|Plantilla|Estado|RazonSocial|NombreComercial|Contacto     |Direccion    |Ciudad|Telefono|Celular    |Correo                     |Vendedor  |ListaPrecio|Zona|
    |CC     |09500945 |15 |Edicion |Inactivo|Edicion   |EDITING Testing    |Mayra Edicion|Calle 1A#23-3|BOG|3700320 |30024687861|edicion@choucairtesting.com|MAY|Lista 1    |2   |
    And creamos un contacto
    |Nombre|Cargo|Telefono|Celular|Correo|
    |Maria Delgado|Ing. Especialista|3690403|30028293484|contacto@alkosto.com|
    And actualizamos el cliente
    
