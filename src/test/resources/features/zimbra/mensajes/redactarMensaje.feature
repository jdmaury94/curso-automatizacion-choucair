@redactarMensaje
Feature: Redactar mensaje
  

  @rutaCritica
  Scenario: Redactar correo y validar al enviar
    Given ingresar al aplicativo zimbra "http://mx1.choucairtesting.com/"
    And loguearse con usuario "scoronado" y contrasenia "123456"
    When ingresar al modulo de mensajes
    And diligencio el formulario con encabezado "prueba 1", cuerpo "mensaje prueba" y correo "scoronado@choucairtesting.com"
    And envio el mensaje
    Then validamos el envio con encabezado "prueba 1", cuerpo "mensaje prueba" y correo "scoronado@choucairtesting.com"
    
