package com.choucair.extragerto.definitions;



import java.util.List;

import com.choucair.extragerto.steps.crearModificarClienteSteps;
import com.choucair.extragerto.steps.menuExtragertoSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class crearModificarClienteDefinition {
	
	@Steps
	crearModificarClienteSteps crearModificarClienteSteps;
	
	@Steps
	menuExtragertoSteps menuExtragertoSteps;

	
	@When("^diligenciamos la informacion del cliente$")
	public void diligenciamos_la_informacion_del_cliente(DataTable listaCampos) {
		
		menuExtragertoSteps.clic_nuevo_cliente();	
		
		List<List<String>> datos=listaCampos.raw();
		System.out.println("Datos .size= "+datos.size());
		for (int i = 1; i < datos.size(); i++) {
			crearModificarClienteSteps.diligenciamos_la_informacion_del_cliente(datos, i);
		}
	}
	
	@When("^creamos el cliente$")
	public void creamos_el_cliente() {
		crearModificarClienteSteps.creamos_el_cliente();
	}
	
	
	@When("^editamos la informacion del cliente$")
	public void editamos_la_informacion_del_cliente(DataTable listaCampos) {
	    // Write code here that turns the phrase above into concrete actions		
		List<List<String>> datos=listaCampos.raw();
		for (int i = 1; i < datos.size(); i++) {
			crearModificarClienteSteps.diligenciamos_la_informacion_del_cliente(datos, i);
		}
	}
	
	
	@When("^actualizamos el cliente$")
	public void actualizamos_el_cliente() {
		crearModificarClienteSteps.actualizamos_el_cliente();
	}

}
