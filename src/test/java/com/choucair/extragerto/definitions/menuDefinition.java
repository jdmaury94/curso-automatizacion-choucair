package com.choucair.extragerto.definitions;

import com.choucair.extragerto.steps.menuExtragertoSteps;

import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class menuDefinition {
	
	@Steps
	menuExtragertoSteps menuSteps;
	
	
	@When("^ingresamos al modulo de ventas/clientes$")
	public void ingresamos_al_modulo_de_ventas_clientes() {
	
		menuSteps.ingresamos_al_modulo_de_ventas_clientes();
		
	}
	
	@When("^validamos mensaje de confirmacion$")
	public void validamos_mensaje_de_confirmacion() {
	   //EN NINGUN MOMENTO APARECE MENSAJE DE CONFIRMACION
	}


	@When("^verificamos que exista usuario con cedula \"([^\"]*)\" en la grilla de clientes$")
	public void verificamos_que_exista_usuario_con_cedula_en_la_grilla_de_clientes(String cedula) {
	    // Write code here that turns the phrase above into concrete actions
		menuSteps.verificamos_que_exista_usuario_con_cedula_en_la_grilla_de_clientes(cedula);
	
	}
	
	
	
	@When("^ingresamos al cliente con cedula \"([^\"]*)\"$")
	public void ingresamos_al_cliente_con_cedula(String cedula) {
	    // Write code here that turns the phrase above into concrete actions
		menuSteps.ingresamos_al_cliente_con_cedula(cedula);
	}
	
	





}
