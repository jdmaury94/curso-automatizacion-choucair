package com.choucair.extragerto.definitions;

import java.util.List;

import com.choucair.extragerto.steps.crearContactoSteps;
import com.choucair.extragerto.steps.crearModificarClienteSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class crearContactoDefinition {
	
	@Steps
	crearContactoSteps crearContactoSteps;
	
	@Steps
	crearModificarClienteSteps crearModificarClienteSteps;
	
	@When("^creamos un contacto$")
	public void creamos_un_contacto(DataTable datosContacto) {
		
	   crearModificarClienteSteps.ingresamos_a_contactos();
	   
	   List<List<String>> datos=datosContacto.raw();
	   
	   for (int i = 1; i < datos.size();i++) {
		   crearContactoSteps.creamos_un_contacto(datos,i);
	   }

	}
	
	

}
