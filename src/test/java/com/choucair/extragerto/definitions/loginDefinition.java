package com.choucair.extragerto.definitions;

import com.choucair.extragerto.steps.loginSteps;

import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;


public class loginDefinition {
	
	@Steps
	loginSteps loginSteps;
	
	@Given("^ingreso al aplicativo \"([^\"]*)\"$")
	public void ingreso_al_aplicativo(String url) {
	    // Write code here that turns the phrase above into concrete actions
		loginSteps.ingreso_al_aplicativo(url);
	}


	@Given("^ingresamos a la sesion de empresa con el usuario \"([^\"]*)\" y la contrasenia \"([^\"]*)\"$")
	public void ingresamos_a_la_sesion_de_empresa_con_el_usuario_y_la_contrasenia(String usuario, String contrasenia) {
	    // Write code here that turns the phrase above into concrete actions
		loginSteps.ingresamos_a_la_sesion_de_empresa_con_el_usuario_y_la_contrasenia(usuario, contrasenia);
	  
	}

	@Given("^ingresamos a la sesion de usuario con el usuario \"([^\"]*)\" y la contrasenia \"([^\"]*)\"$")
	public void ingresamos_a_la_sesion_de_usuario_con_el_usuario_y_la_contrasenia(String usuario, String contrasenia) {
	    // Write code here that turns the phrase above into concrete actions
		loginSteps.ingresamos_a_la_sesion_de_usuario_con_el_usuario_y_la_contrasenia(usuario, contrasenia);
	  
	}

	
}
