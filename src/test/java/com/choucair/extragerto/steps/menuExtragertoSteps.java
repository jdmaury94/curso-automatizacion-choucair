package com.choucair.extragerto.steps;

import com.choucair.extragerto.pageObjects.menuPageObject;

import net.thucydides.core.annotations.Step;

public class menuExtragertoSteps {
	
	menuPageObject menuPageObject;
	
	@Step
	public void ingresamos_al_modulo_de_ventas_clientes() {
		menuPageObject.seleccionarModuloVentas();
		menuPageObject.ingresarModuloClientes();
		
	}
	
	@Step
	public void clic_nuevo_cliente() {
		menuPageObject.clickBotonNuevoCliente();
	}
	
	
	@Step
	public void verificamos_que_exista_usuario_con_cedula_en_la_grilla_de_clientes(String cedula) {
		menuPageObject.ingresarListadoClientes();
		menuPageObject.verificarCreacionCliente(cedula);
		menuPageObject.cerrarSesion();
	}
	
	
	@Step
	public void ingresamos_al_cliente_con_cedula(String cedula) {
		menuPageObject.ingresarAClienteConCedula(cedula);
	}
	

	
	
	
	

}
