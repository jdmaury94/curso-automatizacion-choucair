package com.choucair.extragerto.steps;

import com.choucair.extragerto.pageObjects.loginEmpresaPageObject;
import com.choucair.extragerto.pageObjects.loginUsuarioPageObject;

import net.thucydides.core.annotations.Step;

public class loginSteps {
	
	loginEmpresaPageObject loginEmpresaPageObject;
	loginUsuarioPageObject loginUsuarioPageObject;

	
	@Step
	public void ingreso_al_aplicativo(String url) {
		loginEmpresaPageObject.openAt(url);
	}
	
	@Step
	public void ingresamos_a_la_sesion_de_empresa_con_el_usuario_y_la_contrasenia(String usuario, String contrasenia) {
		loginEmpresaPageObject.ingresarUsuarioSesionEmpresa(usuario);
		loginEmpresaPageObject.ingresarPassSesionEmpresa(contrasenia);
		loginEmpresaPageObject.iniciarSesionEmpresa();
	}
	
	@Step
	public void ingresamos_a_la_sesion_de_usuario_con_el_usuario_y_la_contrasenia(String usuario, String contrasenia) {
		loginUsuarioPageObject.ingresarUsuarioSesionUsuario(usuario);
		loginUsuarioPageObject.ingresarClaveSesionEmpresa(contrasenia);
		loginUsuarioPageObject.iniciarSesionEmpresa();
	}

}
