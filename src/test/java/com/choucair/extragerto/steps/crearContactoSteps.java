package com.choucair.extragerto.steps;

import java.util.List;

import com.choucair.extragerto.pageObjects.crearContactoPageObject;

import net.thucydides.core.annotations.Step;

public class crearContactoSteps {
	
	crearContactoPageObject crearContactoPageObject;
	
	@Step
	public void creamos_un_contacto(List<List<String>> datosContacto, int id) {
		crearContactoPageObject.ingresarNombreContacto(datosContacto.get(id).get(0));
		crearContactoPageObject.ingresarCargoContacto(datosContacto.get(id).get(1));
		crearContactoPageObject.ingresarTelefonoContacto(datosContacto.get(id).get(2));
		crearContactoPageObject.ingresarCelularContacto(datosContacto.get(id).get(3));
		crearContactoPageObject.correoContacto(datosContacto.get(id).get(4));
		crearContactoPageObject.guardarContacto();
	}

}
