package com.choucair.extragerto.steps;

import java.math.BigInteger;
import java.util.List;
import java.util.Random;

import com.choucair.extragerto.pageObjects.crearContactoPageObject;
import com.choucair.extragerto.pageObjects.crearModificarClientePageObject;

import net.thucydides.core.annotations.Step;

public class crearModificarClienteSteps {
	
	crearModificarClientePageObject crearModificarClientePageObject;
	crearContactoPageObject crearContactoPageObject;
	
	@Step
	public void diligenciamos_la_informacion_del_cliente(List<List<String>> listaCampos, int i) {		
		
		Random cedulaRandom=new Random();
		String cedulaBigInteger=new BigInteger(32,cedulaRandom).toString();
		
		crearModificarClientePageObject.selectTipoDoc(listaCampos.get(i).get(0).trim());		

	
		//Estos siempre van-------------------------------------------------------------------

		//crearModificarClientePageObject.ingresarNumDocumento(cedulaBigInteger);
		crearModificarClientePageObject.ingresarNumDocumento(listaCampos.get(i).get(1).trim());
		crearModificarClientePageObject.ingresarFormaDePago(listaCampos.get(i).get(2).trim());
		crearModificarClientePageObject.ingresarPlantilla(listaCampos.get(i).get(3).trim());
		crearModificarClientePageObject.selectEstado(listaCampos.get(i).get(4).trim());
		
		if(!(listaCampos.get(i).get(0).trim().equals("NIT"))) {
				
			crearModificarClientePageObject.ingresarPrimerNombre();
			crearModificarClientePageObject.ingresarSegundoNombre();
			crearModificarClientePageObject.ingresarPrimerApellido();
			crearModificarClientePageObject.ingresarSegundoApellido();
		}			

		else			
			//Esta solo va si es igual a nit
			crearModificarClientePageObject.ingresarRazonSocial(listaCampos.get(i).get(5).trim());
			//Esta solo va si es igual a nit
			
			crearModificarClientePageObject.ingresarNombreComercial(listaCampos.get(i).get(6).trim());
			crearModificarClientePageObject.ingresarContacto(listaCampos.get(i).get(7).trim());
			crearModificarClientePageObject.ingresarDireccion(listaCampos.get(i).get(8).trim());
			crearModificarClientePageObject.ingresarCiudad(listaCampos.get(i).get(9).trim());
			crearModificarClientePageObject.ingresarTelefono(listaCampos.get(i).get(10).trim());
			crearModificarClientePageObject.ingresarCelular(listaCampos.get(i).get(11).trim());
			crearModificarClientePageObject.ingresarCorreo(listaCampos.get(i).get(12).trim());
			crearModificarClientePageObject.ingresarVendedor(listaCampos.get(i).get(13).trim());
			crearModificarClientePageObject.ingresarListaPrecio(listaCampos.get(i).get(14).trim());
			crearModificarClientePageObject.ingresarZona(listaCampos.get(i).get(15).trim());
			//Estos siempre van-------------------------------------------------------------------
	}
	
	@Step
	public void creamos_el_cliente() {
		crearModificarClientePageObject.guardarCliente();
	}
	
	@Step
	public void ingresamos_a_contactos() {
		crearModificarClientePageObject.ingresarAContactos();
		crearContactoPageObject.btnNuevoContacto();
	}
	
	@Step
	public void actualizamos_el_cliente() {
		crearModificarClientePageObject.actualizarCliente();
	}
	


}
