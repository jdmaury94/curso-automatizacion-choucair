package com.choucair.extragerto.pageObjects;


import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class crearContactoPageObject extends PageObject {
	
	@FindBy(id="sc_b_new_t")
	WebElementFacade btnNuevoContacto;
	
	@FindBy(id="id_sc_field_nombre_1")
	WebElementFacade nombreContacto;
	
	@FindBy(id="id_sc_field_cargo_1")
	WebElementFacade cargoContacto;
	
	@FindBy(id="id_sc_field_telefono_1")
	WebElementFacade telefonoContacto;
	
	@FindBy(id="id_sc_field_celular_1")
	WebElementFacade celularContacto;
	
	@FindBy(id="id_sc_field_email_1")
	WebElementFacade correoContacto;
	
	@FindBy(id="id_img_sc_ins_line_1")
	WebElementFacade checkGuardarContacto;
	
	public void btnNuevoContacto() {
		getDriver().switchTo().frame(0);
		btnNuevoContacto.click();
	}
	
	
	public void ingresarNombreContacto(String nombre) {
		nombreContacto.click();
		nombreContacto.sendKeys(nombre);
	}
	
	public void ingresarCargoContacto(String cargo) {
		cargoContacto.click();
		cargoContacto.sendKeys(cargo);
	}
	
	public void ingresarTelefonoContacto(String telefono) {
		telefonoContacto.click();
		telefonoContacto.sendKeys(telefono);
	}
	
	public void ingresarCelularContacto(String celular) {
		celularContacto.click();
		celularContacto.sendKeys(celular);
	}
	
	public void correoContacto(String correo) {
		correoContacto.click();
		correoContacto.sendKeys(correo);
	}
	
	public void guardarContacto() {
		checkGuardarContacto.click();
	}

}
