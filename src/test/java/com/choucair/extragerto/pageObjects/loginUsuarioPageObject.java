package com.choucair.extragerto.pageObjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class loginUsuarioPageObject extends PageObject {
	
	@FindBy(id="id_sc_field_login")
	WebElementFacade txtUsuarioSesionUusario;
	
	@FindBy(id="id_sc_field_pswd")
	WebElementFacade txtClaveSesionUsuario;
	
	@FindBy(xpath="//button[@type='submit']")
	WebElementFacade btnIniciarSesionUsuario;
	
	
	public void ingresarUsuarioSesionUsuario(String usuario) {
		txtUsuarioSesionUusario.sendKeys(usuario);
		
	}
	
	public void ingresarClaveSesionEmpresa(String clave) {
		txtClaveSesionUsuario.sendKeys(clave);
	}
	
	public void iniciarSesionEmpresa() {
		btnIniciarSesionUsuario.click();
	}

}
