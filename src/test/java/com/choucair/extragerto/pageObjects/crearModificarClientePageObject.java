package com.choucair.extragerto.pageObjects;

import org.openqa.selenium.By;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class crearModificarClientePageObject extends PageObject {
	
	@FindBy(id="id_sc_field_tiponit")
	WebElementFacade tipoDoc;
	
	@FindBy(id="id_sc_field_nit")
	WebElementFacade numeroDocumento;
	
	@FindBy(id="id_ac_formadepago_id")
	WebElementFacade formaPago;
	
	@FindBy(id="id_sc_field_estado")
	WebElementFacade listaEstado;
	
	@FindBy(id="id_ac_plantillas_id")
	WebElementFacade txtPlantilla;
	
	@FindBy(id="id_sc_field_razonsocial")
	WebElementFacade txtRazonSocial;
	
	@FindBy(id="id_sc_field_nombre_comercial")
	WebElementFacade txtNombreComercial;
	
	@FindBy(id="id_sc_field_contacto")
	WebElementFacade txtContacto;
	
	@FindBy(id="id_sc_field_direccion")
	WebElementFacade txtDireccion;
	
	@FindBy(id="id_ac_ciudades_id")
	WebElementFacade txtCiudad;
	
	@FindBy(id="id_sc_field_telefono")
	WebElementFacade txtTelefono;
	
	@FindBy(id="id_sc_field_celular")
	WebElementFacade txtCelular;
	
	@FindBy(id="id_sc_field_email")
	WebElementFacade txtCorreo;
	
	@FindBy(id="id_ac_vendedores_id")
	WebElementFacade txtVendedor;
	
	@FindBy(id="id_ac_listasdeprecio_id")
	WebElementFacade txtListaPrecio;
	
	@FindBy(id="id_ac_zonas_id")
	WebElementFacade txtZona;
	
	@FindBy(id="sc_b_ins_t")
	WebElementFacade btnGuardar;
	
	
	@FindBy(id="sc_b_upd_t")
	WebElementFacade btnActualizar;
	
	@FindBy(id="id_sc_field_nombre1")
	WebElementFacade txtPrimerNombre;
	
	@FindBy(id="id_sc_field_nombre2")
	WebElementFacade txtSegundoNombre;
	
	@FindBy(id="id_sc_field_apellido1")
	WebElementFacade txtPrimerApellido;	

	@FindBy(id="id_sc_field_apellido2")
	WebElementFacade txtSegundoApellido;
	
	@FindBy(linkText="Contactos")
	WebElementFacade tabContactos;
	
	public void selectTipoDoc(String tipoDocumento) {
		tipoDoc.click();
		tipoDoc.selectByVisibleText(tipoDocumento);
	}
	
	public void ingresarNumDocumento(String numDocumento) {
		numeroDocumento.click();
		numeroDocumento.sendKeys(numDocumento);
	}
	
	public void ingresarFormaDePago(String formaDePago) {
		formaPago.click();
		formaPago.clear();
		formaPago.sendKeys(formaDePago);
		waitFor(1).seconds();
		find(By.xpath("//li[contains(.,'"+formaPago.getTextValue()+"')]")).click();
	
	}
	
	public void ingresarPlantilla(String plantilla) {
		txtPlantilla.click();
		txtPlantilla.sendKeys(plantilla);
	}
	
	public void selectEstado(String estado) {
		listaEstado.click();
		listaEstado.selectByVisibleText("Activo");
	}
	
	public void ingresarRazonSocial(String razonSocial) {
		txtRazonSocial.click();
		txtRazonSocial.sendKeys(razonSocial);
	}
	
	public void ingresarNombreComercial(String nombreComercial) {
		txtNombreComercial.click();
		txtNombreComercial.sendKeys(nombreComercial);
	}
	
	public void ingresarContacto(String contacto) {
		txtContacto.click();
		txtContacto.sendKeys(contacto);
	}
	
	public void ingresarDireccion(String direccion) {
		txtDireccion.click();
		txtDireccion.sendKeys(direccion);
	}
	
	public void ingresarCiudad(String ciudad) {
		txtCiudad.click();
		txtCiudad.clear();
		//txtCiudad.sendKeys(ciudad);
		txtCiudad.sendKeys(ciudad);
		waitFor(1).seconds();
		find(By.xpath("//li[contains(.,'"+txtCiudad.getTextValue()+"')]")).click();
		
	}
	
	public void ingresarTelefono(String telefono) {
		txtTelefono.click();
		txtTelefono.sendKeys(telefono);
	}
	
	public void ingresarCelular(String celular) {
		txtCelular.click();
		txtCelular.sendKeys(celular);
	}
	
	public void ingresarCorreo(String correo) {
		txtCorreo.click();
		txtCorreo.sendKeys(correo);
	}
	
	public void ingresarVendedor(String vendedor) {
		txtVendedor.click();
		txtVendedor.clear();
		txtVendedor.sendKeys(vendedor);
		waitFor(1).seconds();
		find(By.xpath("//li[contains(.,'"+txtVendedor.getTextValue()+"')]")).click();
	}
	
	public void ingresarListaPrecio(String listaPrecio) {
		txtListaPrecio.click();
		txtListaPrecio.sendKeys(listaPrecio);
		//waitFor(1).seconds();
		//find(By.xpath("//li[contains(.,'"+txtListaPrecio.getTextValue()+"')]")).click();
	}
	
	public void ingresarZona(String zona) {
		txtZona.click();
		txtZona.sendKeys(zona);
		//waitFor(1).seconds();
		//find(By.xpath("//li[contains(.,'"+txtZona.getTextValue()+"')]")).click();
	}
	
	public void guardarCliente() {
		btnGuardar.click();
	}
	
	public void ingresarPrimerNombre() {
		txtPrimerNombre.click();
		txtPrimerNombre.sendKeys("Primer Nombre");
	}
	public void ingresarSegundoNombre() {
		txtSegundoNombre.click();
		txtSegundoNombre.sendKeys("Segundo Nombre");
	}
	public void ingresarPrimerApellido() {
		txtPrimerApellido.click();
		txtPrimerApellido.sendKeys("Primer Apellido");
	}
	public void ingresarSegundoApellido() {
		txtSegundoApellido.click();
		txtSegundoApellido.sendKeys("Segundo Apellido");
	}
	
	public void actualizarCliente() {
		getDriver().switchTo().parentFrame();
		btnActualizar.click();
		waitFor(3).seconds();
	}
	
	public void ingresarAContactos() {
		tabContactos.click();
	}
	

	
}
