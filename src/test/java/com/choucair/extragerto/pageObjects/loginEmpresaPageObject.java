package com.choucair.extragerto.pageObjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class loginEmpresaPageObject extends PageObject{

	@FindBy(id="cred_userid_inputtext")
	WebElementFacade txtUsuarioSesionEmpresa;
	
	@FindBy(id="cred_password_inputtext")
	WebElementFacade txtPassSesionEmpresa;
	
	@FindBy(id="submit")
	WebElementFacade btnSubmit;
	
	public void ingresarUsuarioSesionEmpresa(String usuario) {
		txtUsuarioSesionEmpresa.sendKeys(usuario);
	}
	
	public void ingresarPassSesionEmpresa(String contrasenia) {
		txtPassSesionEmpresa.sendKeys(contrasenia);
	}
	
	public void iniciarSesionEmpresa() {
		btnSubmit.click();
	}
	
}
