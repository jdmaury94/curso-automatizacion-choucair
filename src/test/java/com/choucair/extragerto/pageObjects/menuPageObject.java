package com.choucair.extragerto.pageObjects;


import static org.junit.Assert.assertTrue;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class menuPageObject extends PageObject {
	
	@FindBy(xpath="//a[@id='item_5']/span")
	WebElementFacade moduloVentas;
	
	@FindBy(id="item_7")
	WebElementFacade itmClientes;
	
	@FindBy(id="sc_b_new_top")
	WebElementFacade btnNuevoCliente;
	
	@FindBy(id="item_1_1")
	WebElementFacade btnListaClientes;
	
	
	@FindBy(id="item_380")
	WebElementFacade btnCerrarSesion;
	

	
	public void seleccionarModuloVentas() {
		moduloVentas.click();
	}
	
	public void ingresarModuloClientes() {
		itmClientes.click();
	}
	
	public void clickBotonNuevoCliente() {
		//btnNuevoCliente.waitUntilClickable();
		//getDriver().switchTo().frame(0);
		getDriver().switchTo().frame(17);
		getDriver().switchTo().frame(0);
		//getDriver().findElement(By.id("sc_b_new_top")).click();
		//btnNuevoCliente.waitUntilClickable();
		btnNuevoCliente.click();
	}
	
	public void ingresarListadoClientes() {
		getDriver().switchTo().parentFrame();
		//getDriver().findElement(By.id("item_1_1")).click();
		//getDriver().findElement(By.xpath("//a[contains(.,\'Listado de clientes\')]")).click();
		btnListaClientes.click();
	}
	
	public void verificarCreacionCliente(String cedula) {
		getDriver().switchTo().frame(2);
		boolean existe=getDriver().findElements(By.xpath("//tr//td[4]//span[contains(.,'"+cedula+"')]")).size()>0;
		assertTrue(existe);
		////tr//td[4]//span[contains(.,"1.045.721.591")]
		////find(By.xpath("//li[contains(.,'"+txtZona.getTextValue()+"')]")).click();
	}
	
	public void cerrarSesion() {
		getDriver().switchTo().parentFrame();
		getDriver().switchTo().parentFrame();
		btnCerrarSesion.click();
	}
	
	
	public void ingresarAClienteConCedula(String cedula) {
		getDriver().switchTo().frame(17);
		getDriver().switchTo().frame(0);
		getDriver().findElement(By.xpath("//td[4][contains(.,'"+cedula+"')]//preceding-sibling::td[2]")).click();
		waitFor(2).seconds();
	}
	
	

}
