package com.choucair.zimbra.steps;

import com.choucair.zimbra.pageObjects.loginPageObject;

import net.thucydides.core.annotations.Step;

public class loginSteps {
	
	loginPageObject loginPageObject;

	@Step
	public void ingresar_al_aplicativo_zimbra(String strURL) {
		loginPageObject.openAt(strURL);
	}
	
	@Step
	public void loguearse_con_usuario_y_contrasenia(String strUsuario, String strContrasenia) {
		loginPageObject.ingresarUsuario(strUsuario);
		loginPageObject.ingresarContrasenia(strContrasenia);
		loginPageObject.clickBoton();
	}

}
