package com.choucair.zimbra.pageObjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class redactarMensajePageObject extends PageObject {

	@FindBy(id="1")
	WebElementFacade txtUsuarioPF;
	
	String txtUsuarioN="1"; //id
	
	public void digite_nombre_de_usuario(String strUsuario) {
		
		txtUsuarioPF.sendKeys(strUsuario);
		//getDriver().findElement(By.id(txtUsuarioN)).sendKeys(strUsuario);
		//find(By.id(txtUsuarioN)).sendKeys(strUsuario);
	}

}
