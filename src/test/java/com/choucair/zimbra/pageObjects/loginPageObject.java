package com.choucair.zimbra.pageObjects;




import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class loginPageObject extends PageObject{
	
	@FindBy(id="username")
	WebElementFacade txtUsuario;
	
	@FindBy(id="password")
	WebElementFacade txtContrasenia;
	
	@FindBy(xpath="/html/body/div/div[1]/form/table/tbody/tr[3]/td[2]/input[1]")
	WebElementFacade btnIniciarSesion;
	
	public void ingresarUsuario(String usuario) {
		try {
		txtUsuario.sendKeys(usuario);	
		
		}
		catch (java.lang.AssertionError e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	
	public void ingresarContrasenia(String contrasenia) {
		txtContrasenia.sendKeys(contrasenia);
	}
	
	public void clickBoton() {
		btnIniciarSesion.click();
	}

	

}
