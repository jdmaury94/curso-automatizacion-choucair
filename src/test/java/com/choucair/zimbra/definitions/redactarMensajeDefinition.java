package com.choucair.zimbra.definitions;

import com.choucair.zimbra.steps.redactarMensajeSteps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class redactarMensajeDefinition {
	
	@Steps
	redactarMensajeSteps redactarMensajeSteps;

	@When("^diligencio el formulario con encabezado (.*), cuerpo \"([^\"]*)\" y correo \"([^\"]*)\"$")
	public void diligencio_el_formulario_con_encabezado_cuerpo_y_correo(String strEncabezado, String strMensaje, String strCorreo) {
		
	}
	
	@When("^envio el mensaje")
	public void envio_el_mensaje() {
		
	}
	
	@Then("^validamos el envio con encabezado \"([^\"]*)\", cuerpo \"([^\"]*)\" y correo \"([^\"]*)\"$")
	public void validamos_el_envio_con_encabezado_cuerpo_y_correo(String strEncabezado, String strMensaje, String strCorreo) {
		
	}
}
