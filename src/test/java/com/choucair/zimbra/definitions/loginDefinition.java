package com.choucair.zimbra.definitions;

import com.choucair.zimbra.steps.loginSteps;

import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class loginDefinition {
	
	@Steps
	loginSteps loginSteps;
//
	@Given("^ingresar al aplicativo zimbra \"([^\"]*)\"$")
	public void ingresar_al_aplicativo_zimbra(String strURL) {
		loginSteps.ingresar_al_aplicativo_zimbra(strURL);
		
	}
	
	@Given("^loguearse con usuario \"([^\"]*)\" y contrasenia \"([^\"]*)\"$")
	public void loguearse_con_usuario_y_contrasenia(String strUsuario, String strContrasenia) {
		loginSteps.loguearse_con_usuario_y_contrasenia(strUsuario, strContrasenia);
	}


	
	
	

}
